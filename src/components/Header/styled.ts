import styled from 'styled-components';
import { COLOR_ACCENT, COLOR_GREY, COLOR_WHITE } from '../../constants';

export const Header = styled.header`
  display: flex;
  align-items: center;
  padding: 15px 20px;
  gap: 25px;
  background-color: ${COLOR_WHITE};
  box-shadow: 0px 0px 5px ${COLOR_GREY};
  line-height: 1;
`;

export const HeaderTitle = styled.span`
  margin-right: 20px;
  font-size: 1.5rem;
  letter-spacing: -0.5px;
  font-weight: bold;
  color: ${COLOR_ACCENT};
`;

export const UsersCount = styled.span`
  &::after {
    content: 'participants';
    margin-left: 5px;
  }
`;

export const MessagesCount = styled.span`
  &::after {
    content: 'messages';
    margin-left: 5px;
  }
`;

export const LastMessageDate = styled.span`
  flex-grow: 1;
  text-align: right;
  &::before {
    content: 'last message at';
    margin-right: 5px;
  }
`;
