import { RootState } from '../../types';

export const selectMessages = (state: RootState) => state.chat.messages;
export const selectMessage = (state: RootState, messageId: string) =>
  state.chat.messages.find(({ id }) => id === messageId);
export const selectEditedMessageId = (state: RootState) => state.chat.editedMessageId;
export const selectPreloader = (state: RootState) => state.chat.preloader;
export const selectEditModal = (state: RootState) => state.chat.editModal;
