import styled, { createGlobalStyle } from 'styled-components';
import { COLOR_ACCENT, COLOR_BACKGROUND } from '../../constants';

export const GlobalStyle = createGlobalStyle`
::-webkit-scrollbar {
  width: 14px;
}
::-webkit-scrollbar-thumb {
  border: 4px solid rgba(0, 0, 0, 0);
  background-clip: padding-box;
  background-color: ${COLOR_ACCENT};
  border-radius: 10px;
}
`;

export const Chat = styled.div`
  height: 100vh;
  padding: 15px;
  overflow: hidden;
  background-color: ${COLOR_BACKGROUND};
`;

export const Container = styled.div`
  height: 100%;
  width: 100%;
  max-width: 1200px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  gap: 10px;
`;
