import { FC } from 'react';

import * as S from './styled';

export const Preloader: FC = () => {
  return (
    <S.Preloader className="preloader">
      <S.PreloaderIcon />
    </S.Preloader>
  );
};
