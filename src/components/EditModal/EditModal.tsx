import { useEffect } from 'react';
import { ChangeEvent, FC, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../types';
import { resetEditedMessage, setMessage } from '../Chat/chatActions';
import { selectEditedMessageId, selectEditModal, selectMessage } from '../Chat/chatSelectors';
import * as S from './styled';

export const EditModal: FC = () => {
  const dispatch = useDispatch();

  const [text, setText] = useState('');
  const isShown = useSelector(selectEditModal);
  const editedMessageId = useSelector(selectEditedMessageId) || '';
  const editedMessage = useSelector((state: RootState) => selectMessage(state, editedMessageId));

  const handleTextChange = (e: ChangeEvent<HTMLTextAreaElement>) => setText(e.target.value);

  const hideModal = () => dispatch(resetEditedMessage());

  const handleSave = () => {
    if (!editedMessage || !text.trim()) return;

    dispatch(setMessage({ ...editedMessage, text }));
    hideModal();
  };

  useEffect(() => {
    if (editedMessage) {
      setText(editedMessage.text);
    }
  }, [editedMessage]);

  return (
    <S.ModalContainer isShown={isShown}>
      <S.Modal className={`edit-message-modal ${isShown && 'modal-shown'}`}>
        <S.ModalHeader>
          <S.ModalTitle>Edit Message</S.ModalTitle>
        </S.ModalHeader>
        <S.ModalBody>
          <S.ModalTextarea
            className="edit-message-input"
            placeholder="Write Something..."
            onChange={handleTextChange}
            value={text}
          />
        </S.ModalBody>
        <S.ModalFooter>
          <S.ModalButton accent className="edit-message-button" onClick={handleSave}>
            Save
          </S.ModalButton>
          <S.ModalButton className="edit-message-close" onClick={hideModal}>
            Cancel
          </S.ModalButton>
        </S.ModalFooter>
      </S.Modal>
    </S.ModalContainer>
  );
};
