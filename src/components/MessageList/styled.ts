import styled from 'styled-components';
import { COLOR_FONT_GREY, COLOR_GREY, COLOR_WHITE } from '../../constants';

export const MessageList = styled.div`
  width: 100%;
  padding: 15px 20px;
  background-color: ${COLOR_WHITE};
  box-shadow: 0px 0px 5px ${COLOR_GREY};
  overflow: auto;
`;

export const MessagesDivider = styled.div`
  display: flex;
  align-items: center;
  margin: 20px 0;
  color: ${COLOR_FONT_GREY};
  line-height: 1;
  &::before,
  &::after {
    content: '';
    height: 1px;
    flex-grow: 1;
    margin-right: 15px;
    background-color: ${COLOR_GREY};
    transform: translateY(1px);
  }
  &::after {
    margin-left: 15px;
  }
`;
