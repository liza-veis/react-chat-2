import { FC, useState } from 'react';
import { AiFillHeart, AiOutlineHeart } from 'react-icons/ai';
import { MESSAGE_TIME_FORMAT } from '../../constants';
import { formatDate } from '../../helpers';
import { MessageData } from '../../types';
import * as S from './styled';

type MessageProps = {
  data: MessageData;
};

export const Message: FC<MessageProps> = ({ data }) => {
  const { avatar, user, text, createdAt } = data;

  const [isLiked, setIsLiked] = useState(false);

  const time = formatDate(createdAt, MESSAGE_TIME_FORMAT);

  const handleButtonLikeClick = () => setIsLiked(!isLiked);

  return (
    <S.Message className="message">
      <S.MessageUserAvatar className="message-user-avatar" src={avatar} alt={user} />
      <S.MessagesContent className="message-content">
        <S.MessageUserName className="message-user-name">{user}</S.MessageUserName>
        <S.MessageText className="message-text">{text}</S.MessageText>
      </S.MessagesContent>
      <S.MessageAside className="message-aside">
        <S.MessageTime className="message-time">{time}</S.MessageTime>
        <S.ButtonLike
          className={isLiked ? 'message-liked' : 'message-like'}
          onClick={handleButtonLikeClick}
        >
          {isLiked ? <AiFillHeart /> : <AiOutlineHeart />}
        </S.ButtonLike>
      </S.MessageAside>
    </S.Message>
  );
};
