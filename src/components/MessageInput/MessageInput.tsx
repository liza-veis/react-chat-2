import { FC, useState, ChangeEvent, FormEvent } from 'react';
import { MdSend } from 'react-icons/md';
import { useDispatch } from 'react-redux';
import { createMessage } from '../../helpers';
import { addMessage } from '../Chat/chatActions';
import * as S from './styled';

export const MessageInput: FC = () => {
  const dispatch = useDispatch();

  const [text, setText] = useState('');

  const handleTextChange = (e: ChangeEvent<HTMLInputElement>) => setText(e.target.value);

  const handleAddMessage = (e: FormEvent) => {
    e.preventDefault();
    if (!text.trim()) return;

    const message = createMessage({ text });

    dispatch(addMessage(message));
    setText('');
  };

  return (
    <S.MessageInput className="message-input">
      <S.MessageInputText
        className="message-input-text"
        placeholder="Write Something"
        onChange={handleTextChange}
        value={text}
      />
      <S.MessageInputButton
        className="message-input-button"
        type="submit"
        title="Send"
        onClick={handleAddMessage}
      >
        <MdSend />
      </S.MessageInputButton>
    </S.MessageInput>
  );
};
