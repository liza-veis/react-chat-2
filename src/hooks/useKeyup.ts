import { useEffect } from 'react';

export const useKeyup = (key: string, callback: () => void) => {
  useEffect(() => {
    const onKeyup = (e: KeyboardEvent) => {
      e.preventDefault();

      if (e.key === key) callback();
    };

    window.addEventListener('keyup', onKeyup);

    return () => window.removeEventListener('keyup', onKeyup);
  }, [key, callback]);
};
