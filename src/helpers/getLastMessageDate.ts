import { AnyMessageData } from '../types';
import { sortMessagesByDate } from './sortMessagesByDate';

export const getLastMessageDate = (messages: AnyMessageData[]) => {
  const sortedMessages = sortMessagesByDate(messages).reverse();
  return sortedMessages[0]?.createdAt || '';
};
