import { AnyMessageData, SplitMessagesData } from '../types';
import { formatDateToCalendar } from './formatDateToCalendar';
import { sortMessagesByDate } from './sortMessagesByDate';

export const splitMessagesByDate = (messages: AnyMessageData[]) => {
  const splitMessages: SplitMessagesData = {};
  const sortedMessages = sortMessagesByDate(messages);

  sortedMessages.forEach((message) => {
    const { createdAt } = message;
    const date = formatDateToCalendar(createdAt);

    if (!splitMessages[date]) {
      splitMessages[date] = [];
    }

    splitMessages[date].push(message);
  });

  return splitMessages;
};
