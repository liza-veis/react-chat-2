import { AnyMessageData } from '../types';
import { sortMessagesByDate } from './sortMessagesByDate';

export const getLastOwnMessage = (messages: AnyMessageData[]) => {
  const sortedMessages = sortMessagesByDate(messages).reverse();
  return sortedMessages.find((message) => !('user' in message));
};
