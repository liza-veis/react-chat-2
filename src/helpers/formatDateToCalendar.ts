import moment from 'moment';
import { MESSAGE_LIST_DATE_FORMAT } from '../constants';

export const formatDateToCalendar = (date: string) =>
  moment(new Date(date)).calendar(null, {
    sameDay: '[Today]',
    lastDay: '[Yesterday]',
    lastWeek: MESSAGE_LIST_DATE_FORMAT,
    sameElse: MESSAGE_LIST_DATE_FORMAT,
  });
